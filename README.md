This is a simple script to fetch related work from google scholar, and create a summary spreadsheet. 
It can be customised by changing the values of the static variables in `nz.ac.wgtn.ecs.veracity.scholar_scraper.Main`.
It uses selenium, and you will need to set the `SELENIUM_DRIVER` value to a driver that matches the OS / browser used.

The easiest way to run this is to load this into a Java IDE such as IntelliJ, and run `nz.ac.wgtn.ecs.veracity.scholar_scraper.Main`.

