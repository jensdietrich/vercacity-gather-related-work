package nz.ac.wgtn.ecs.veracity.scholar_scraper;

import org.apache.http.client.utils.URIBuilder;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Collects the top 10 publications using scholars default ranking for a search term over a couple of years.
 * TODO: expand this with an additional query parameter to fetch multiple pages of 10 for each year.
 * @author jens dietrich
 */
public class Main {

    public static final String SELENIUM_DRIVER = "chrome-driver/osx/chrome-90/chromedriver";

    public static final String HOST = "scholar.google.com";
    public static final String SCHEME = "https";
    public static final String PATH = "scholar";
    public static final String QUERY = "provenance";
    public static final int START_YEAR = 2007;
    public static final int END_YEAR = 2021;

    // keys to be used as column names
    public static final String COL_YEAR = "year";
    public static final String COL_TITLE = "title";
    public static final String COL_LINK = "link";
    public static final String COL_AUTHORS_AND_VENUES_SHORT = "authors-venue-short";
    public static final String COL_CITATIONS= "citations";
    public static final String COL_ABSTARCT = "abstract";
    public static final String COL_EXTRACTION_TIMESTAMP = "prov-timestamp";
    public static final String COL_EXTRACTION_METHOD = "prov-method";
    public static final String COL_EXTRACTION_CREATOR = "prov-creator";

    public static final DateFormat TIMESTAMP_FORMAT = DateFormat.getDateTimeInstance();

    public static final String[] COLUMNS = new String[] {
            COL_YEAR, COL_TITLE, COL_LINK, COL_AUTHORS_AND_VENUES_SHORT, COL_CITATIONS, COL_ABSTARCT,
            COL_EXTRACTION_TIMESTAMP, COL_EXTRACTION_METHOD, COL_EXTRACTION_CREATOR
    };

    public static final String[] PREFIX_TO_BE_REMOVED_FROM_TITLE = {
            "[BOOK]","[PDF]","[HTML]"
    };

    public static final File RESULT_FOLDER = new File("results");
    public static final File RESULT_FILE = new File(RESULT_FOLDER,"results.csv");
    public static final String CSV_SEP = "\t";

    static {
        System.setProperty("webdriver.chrome.driver",SELENIUM_DRIVER);
        System.out.println("Using driver: " + SELENIUM_DRIVER + (" note: platform and chrome version must match !"));
    }

    public static void main (String[] args) throws Exception {
        if (!RESULT_FOLDER.exists()) {
            RESULT_FOLDER.mkdirs();
        }
        try (PrintWriter out = new PrintWriter(new FileWriter(RESULT_FILE))) {
            out.println(Stream.of(COLUMNS).collect(Collectors.joining(CSV_SEP)));
        }

        for (int year = START_YEAR;year<=END_YEAR;year++) {
            String url = new URIBuilder()
                .setHost(HOST)
                .setScheme(SCHEME)
                .setPath(PATH)
                .addParameter("q", QUERY)
                .addParameter("hl", "en")
                .addParameter("as_ylo", "" + year)
                .addParameter("as_yhi", "" + year)
                .toString();
            System.out.println("extracting links from: " + url);

            List<String> lines = new ArrayList<>();
            WebDriver driver = new ChromeDriver();
            driver.get(url);
            List<WebElement> elements = driver.findElements(By.cssSelector(".gs_ri"));
            for (WebElement element : elements) {
                Map<String, String> data = new HashMap<>();
                data.put(COL_YEAR,""+year);
                data.put(COL_EXTRACTION_TIMESTAMP,TIMESTAMP_FORMAT.format(new Date()));
                data.put(COL_EXTRACTION_METHOD,Main.class.getName());
                data.put(COL_EXTRACTION_CREATOR,System.getProperty("user.name"));

                // extract title and link
                WebElement titleWebElement = element.findElements(By.cssSelector(".gs_rt")).get(0);
                String title = titleWebElement.getText();
                for (String prefix:PREFIX_TO_BE_REMOVED_FROM_TITLE) {
                    if (title.startsWith(prefix)) {
                        title = title.substring(prefix.length()).trim();
                    }
                }

                data.put(COL_TITLE, title);
                WebElement linkElement = element.findElements(By.cssSelector("a")).get(0);
                String link = linkElement.getAttribute("href");
                data.put(COL_LINK, link);

                // extract author and venue (possibly incomplete, only first authors)
                WebElement authorAndVenueShortWebElement = element.findElements(By.cssSelector(".gs_a")).get(0);
                String authors = authorAndVenueShortWebElement.getText();
                data.put(COL_AUTHORS_AND_VENUES_SHORT,authors);

                // extract abstract
                WebElement abstractWebElement = element.findElements(By.cssSelector(".gs_rs")).get(0);
                String abstractTxt = abstractWebElement.getText();
                abstractTxt = abstractTxt.replace(System.lineSeparator(),"\\\\");
                data.put(COL_ABSTARCT,abstractTxt);

                // extract citations
                WebElement buttomWebElement = element.findElements(By.cssSelector(".gs_fl")).get(0);
                List<WebElement> linkElements = buttomWebElement.findElements(By.cssSelector("a"));
                for (WebElement linkElement2:linkElements) {
                    if (linkElement2.getText().startsWith("Cited by")) {
                        String citationsTxt = linkElement2.getText().substring(8).trim();
                        int citations = Integer.parseInt(citationsTxt);
                        data.put(COL_CITATIONS,""+citations);
                    }
                }

                lines.add(Stream.of(COLUMNS).map(col -> data.get(col)).collect(Collectors.joining(CSV_SEP)));

                System.out.println("Extracted " + authors + ": " + title + " [" + year + "]");
                System.out.println();
            }

            try (PrintWriter out = new PrintWriter(new FileWriter(RESULT_FILE, true))) {
                for (String line : lines) {
                    out.println(line);
                }
            }
            driver.close();
        }



    }
}
